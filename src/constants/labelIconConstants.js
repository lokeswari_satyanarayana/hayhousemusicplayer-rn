/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

export const labelIcons = {
    speed: {
        icon: require('../assets/speedControl.png'),
        label: 'Speed'
    },
    favorite: {
        icon: require('../assets/fav.png'),
        label: 'Favorites'
    },
    devices: {
        icon: require('../assets/audio.png'),
        label: 'Devices'
    },
    download: {
        icon: require('../assets/download.png'),
        label: 'Download'
    },
    chapters: {
        icon: require('../assets/list.png'),
        label: 'Chapters'
    }
}