/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

export const colors = {
    slateGrey: '#4e4c62',
    coralPink: '#fb6a6d',
    salmon: '#ff7775',
    duskTwo: '#4e4a64',
}