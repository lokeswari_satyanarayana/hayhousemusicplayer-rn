/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import LabelIcon from './LabelIcon';
import { labelIcons } from '../constants/labelIconConstants';

const AudioPlayerControls = ({ paused, didPause, skipForward, skipBackward }) => {

    const icon = () => {
        if (paused) {
            return require('../assets/play.png')
        } else {
            return require('../assets/pause.png')
        }
    };

    const controls = () => {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    onPress={skipBackward}>
                    <Image
                        style={styles.icon}
                        source={require('../assets/backward.png')} />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={skipBackward}>
                    <Image
                        style={styles.icon}
                        source={require('../assets/back15.png')} />
                </TouchableOpacity>
                <View
                    style={{
                        height: 60,
                        width: 60,
                        borderRadius: 30,
                        backgroundColor: 'black',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                    <TouchableOpacity
                        onPress={didPause}>
                        <Image
                            style={styles.centerIcon}
                            source={icon()}
                        />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    onPress={skipForward}>
                    <Image
                        style={styles.icon}
                        source={require('../assets/for15.png')} />
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image
                        style={styles.icon}
                        source={require('../assets/forward.png')} />
                </TouchableOpacity>
            </View>
        );
    };

    const options = () => {
        return (
            <View style={styles.container}>
                <LabelIcon labelItem={labelIcons.speed} />
                <LabelIcon labelItem={labelIcons.favorite} />
                <LabelIcon labelItem={labelIcons.devices} />
                <LabelIcon labelItem={labelIcons.download} />
                <LabelIcon labelItem={labelIcons.chapters} />
            </View>
        );
    }

    return (
        <View>
            {controls()}
            {options()}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        opacity: 0.8,
        borderRadius: 30,
        width: '80%',
        alignSelf: 'center',
        marginVertical: 20
    },
    icon: {
        tintColor: 'black',
        height: 22,
        width: 22,
        marginHorizontal: 10,
    },
    centerIcon: {
        tintColor: 'white',
        height: 22,
        width: 22,
        alignSelf: 'center'
    }
})

export default AudioPlayerControls;