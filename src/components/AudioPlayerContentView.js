import React, { useState, useRef } from 'react';
import { View, Image, StyleSheet, Dimensions, Text } from 'react-native';
import AudioPlayerControls from './AudioPlayerControls';
import Audio from 'react-native-video';
import { colors } from '../constants/colors';
import Animated from 'react-native-reanimated';
import Slider from '@react-native-community/slider';
import { ScrollView } from 'react-native-gesture-handler';

const AudioPlayerContentView = () => {

    const audioPlayerInitialState = {
        currentTime: 0.00,
        duration: 0.00
    }

    const [pause, setPause] = useState(true);
    const [playerState, setPlayerState] = useState(audioPlayerInitialState);
    const audioPlayer = useRef(null);

    const audio = () => {
        return <Audio
            ref={audioPlayer}
            audioOnly={true}
            playInBackground={true}
            repeat={false}
            paused={pause}
            onLoad={onLoadEnd}
            onProgress={onProgress}
            onEnd={onEnd}
            source={require('../assets/allIWant.mp3')}
        />
    };

    const topBar = () => {
        return (
            <View style={styles.topBarContainer} >
                <Image
                    style={styles.topBarIcons}
                    source={require('../assets/info.png')} />
                <Image
                    style={styles.topBarIcons}
                    source={require('../assets/collapse.png')} />
                <Image
                    style={styles.topBarIcons}
                    source={require('../assets/share.png')} />
            </View>
        );
    };

    const thumbnail = () => {
        return (
            <View
                style={styles.imageContainer}>
                <Image
                    style={styles.image}
                    source={require('../assets/allIWant.jpg')} />
            </View>
        );
    };

    const title = () => {
        return (
            <Text
                style={styles.title}>
                You Can Heal Your Life - Chapter 1
            </Text>
        );
    };

    const subtitle = () => {
        return (
            <Text
                style={styles.subtitle}>
                Kodaline
            </Text>
        );
    }

    const controls = () => {
        return (
            <AudioPlayerControls
                paused={pause}
                didPause={() => { setPause(!pause) }}
                skipBackward={() => skipBackward()}
                skipForward={() => skipForward()}
            />
        );
    }

    const slider = () => {
        return (
            <View
                style={styles.sliderContainer}>
                <View
                    style={styles.sliderLabelContainer}>
                    <Text
                        style={styles.sliderLabel}>
                        {formatted(playerState.currentTime)}
                    </Text>
                    <Text
                        style={styles.sliderLabel}>
                        {formatted(playerState.duration)}
                    </Text>
                </View>
                <Slider
                    style={styles.slider}
                    thumbImage={require('../assets/oval.png')}
                    value={playerState.currentTime}
                    onValueChange={onValueChange}
                    minimumValue={0}
                    maximumValue={playerState.duration}
                    minimumTrackTintColor="#000000"
                    maximumTrackTintColor="#A9A9A9"
                />
            </View>
        );
    }

    const onLoadEnd = (data) => {
        setPlayerState(currentState => ({
            ...currentState,
            duration: data.duration,
            currentTime: data.currentTime,
        }));
    }

    const skipBackward = () => {
        audioPlayer.current.seek(playerState.currentTime - 15)
        setPlayerState(currentState => ({
            ...currentState,
            currentTime: currentState.currentTime - 15,
        }));
    }

    const skipForward = () => {
        audioPlayer.current.seek(playerState.currentTime + 15)
        setPlayerState(currentState => ({
            ...currentState,
            currentTime: currentState.currentTime + 15,
        }));
    }

    const onProgress = (data) => {
        setPlayerState(currentState => ({
            ...currentState,
            currentTime: data.currentTime,
        }));
    }

    const onEnd = () => {
        audioPlayer.current.seek(0);
    }

    const onValueChange = (time) => {
        audioPlayer.current.seek(time)
        setPlayerState(currentState => ({
            ...currentState,
            currentTime: time,
        }));
    }

    const formatted = (time) => {
        const minutes = time >= 60 ? Math.floor(time / 60) : 0;
        const seconds = Math.floor(time - minutes * 60);
        return `${minutes >= 10 ? minutes : '0' + minutes}:${seconds >= 10 ? seconds : '0' + seconds}`;
    }

    return <View style={styles.viewContainer}>
        {audio()}
        {topBar()}
        {thumbnail()}
        <View
            style={styles.contentContainer}>
            {title()}
            {subtitle()}
            {slider()}
        </View>
        {controls()}
    </View>

}

const styles = StyleSheet.create({
    viewContainer: {
        height: '100%',
        width: '100%',
        backgroundColor: 'white',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    slider: {
        height: 1
    },
    sliderLabelContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 15,
    },
    sliderLabel: {
        fontSize: 13,
        color: 'gray',
        fontWeight: 'bold'
    },
    topBarContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        marginTop: 12,
        marginBottom: 14,
        marginHorizontal: 12
    },
    topBarIcons: {
        height: 20,
        width: 20,
        tintColor: 'black'
    },
    imageContainer: {
        height: 300,
        width: 300,
        alignSelf: 'center',
        aspectRatio: 1
    },
    image: {
        flex: 1,
        height: '100%',
        width: '100%',
        resizeMode: 'contain',
    },
    title: {
        fontWeight: 'bold',
        fontSize: 22,
        marginBottom: 12
    },
    subtitle: {
        color: colors.salmon,
        fontWeight: 'bold',
        fontSize: 16
    },
    contentContainer: {
        marginTop: 71,
        marginHorizontal: 20,
    },
    sliderContainer: {
        marginTop: 23,
        justifyContent: 'center',
        width: '100%',
        height: 31.5,
        marginBottom: 63
    }
});

export default AudioPlayerContentView;