/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import colors from '../constants/colors'
import { TouchableOpacity } from 'react-native-gesture-handler';

const LabelIcon = ({ labelItem }) => {
    return (
        <View>
            <View
                style={{
                    backgroundColor: 'rgba(78,76,98,0.20)',
                    height: 44,
                    width: 44,
                    alignItems: 'center',
                    justifyContent: 'center'
                }} >
                <TouchableOpacity>
                    <Image
                        style={styles.icon}
                        source={labelItem.icon}
                    />
                </TouchableOpacity>
            </View>
            <Text
                style={styles.label}>
                {labelItem.label}
            </Text>
        </View>
    );
}

const styles = StyleSheet.create({
    icon: {
        tintColor: 'black',
        height: 22,
        width: 22,
        alignSelf: 'center',
    },
    label: {
        fontSize: 12,
        textAlign: 'center'
    }
})

export default LabelIcon;