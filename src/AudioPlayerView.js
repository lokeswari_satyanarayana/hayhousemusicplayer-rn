/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { View, Dimensions } from 'react-native';
import AudioPlayerControls from './components/AudioPlayerControls';
import AudioPlayerContentView from './components/AudioPlayerContentView';

const AudioPlayerView = () => {

    return (
        <View
            style={{
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height,
                flex: 1,
                justifyContent: 'flex-end',
                backgroundColor: 'black',
            }}>
            <View style={{
                width: '100%',
                height: '98%',
                justifyContent: 'flex-end',
                borderWidth: 1,
            }}>
                <AudioPlayerContentView />
            </View>
        </View >
    );
}

export default AudioPlayerView;